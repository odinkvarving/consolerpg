using ConsoleRPG.CharacterTypes;

namespace ConsoleRPGTests
{
    public class CharacterTests
    {

        [Fact]
        public void CreateNewCharacter_CheckInitialCharacterLevel_ShouldBe1()
        {
            Mage mage = new Mage("Gandalf");

            int level = mage.Level;

            Assert.Equal(1, level);
        }

        [Fact]
        public void LevelUp_UseLevelUpMethodOnCharacter_ShouldIncreaseByOne()
        {
            Mage mage = new Mage("Gandalf");

            mage.LevelUp();
            int level = mage.Level;

            Assert.Equal(2, level);
        }

        [Fact]
        public void CreateNewMage_CheckInitialPrimaryAttributesWhenCreatingNewMage_ShouldBeDefaultValues()
        {
            Mage mage = new Mage("Gandalf");

            (int strength, int dexterity, int intelligence) actualAttributes = (mage.PrimaryAttributes.Strength, mage.PrimaryAttributes.Dexterity, mage.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (1, 1, 8);
           
            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void CreateNewRanger_CheckInitialPrimaryAttributesWhenCreatingNewRanger_ShouldBeDefaultValues()
        {
            Ranger ranger = new Ranger("Legolas");

            (int strength, int dexterity, int intelligence) actualAttributes = (ranger.PrimaryAttributes.Strength, ranger.PrimaryAttributes.Dexterity, ranger.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (1, 7, 1);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void CreateNewRogue_CheckInitialPrimaryAttributesWhenCreatingNewRogue_ShouldBeDefaultValues()
        {
            Rogue rogue = new Rogue("Gollum");

            (int strength, int dexterity, int intelligence) actualAttributes = (rogue.PrimaryAttributes.Strength, rogue.PrimaryAttributes.Dexterity, rogue.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (2, 6, 1);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void CreateNewWarrior_CheckInitialPrimaryAttributesWhenCreatingNewWarrior_ShouldBeDefaultValues()
        {
            Warrior warrior = new Warrior("Aragorn");

            (int strength, int dexterity, int intelligence) actualAttributes = (warrior.PrimaryAttributes.Strength, warrior.PrimaryAttributes.Dexterity, warrior.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (5, 2, 1);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_CheckIfMagePrimaryAttributesIncreaseCorrectly_ShouldIncreaseByCorrectRatio()
        {
            Mage mage = new Mage("Gandalf");

            mage.LevelUp();

            (int strength, int dexterity, int intelligence) actualAttributes = (mage.PrimaryAttributes.Strength, mage.PrimaryAttributes.Dexterity, mage.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (2, 2, 13);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_CheckIfRangerPrimaryAttributesIncreaseCorrectly_ShouldIncreaseByCorrectRatio()
        {
            Ranger ranger = new Ranger("Legolas");

            ranger.LevelUp();

            (int strength, int dexterity, int intelligence) actualAttributes = (ranger.PrimaryAttributes.Strength, ranger.PrimaryAttributes.Dexterity, ranger.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (2, 12, 2);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_CheckIfRoguePrimaryAttributesIncreaseCorrectly_ShouldIncreaseByCorrectRatio()
        {
            Rogue rogue = new Rogue("Gollum");

            rogue.LevelUp();

            (int strength, int dexterity, int intelligence) actualAttributes = (rogue.PrimaryAttributes.Strength, rogue.PrimaryAttributes.Dexterity, rogue.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (3, 10, 2);

            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_CheckIfWarriorPrimaryAttributesIncreaseCorrectly_ShouldIncreaseByCorrectRatio()
        {
            Warrior warrior = new Warrior("Aragorn");

            warrior.LevelUp();

            (int strength, int dexterity, int intelligence) actualAttributes = (warrior.PrimaryAttributes.Strength, warrior.PrimaryAttributes.Dexterity, warrior.PrimaryAttributes.Intelligence);
            (int solutionStrength, int solutionDexterity, int solutionIntelligence) expectedAttributes = (8, 4, 2);

            Assert.Equal(expectedAttributes, actualAttributes);
        }
    }
}