﻿using ConsoleRPG.CharacterTypes;
using ConsoleRPG.CustomExceptions;
using ConsoleRPG.CommonAttributes;
using ConsoleRPG.Items;

namespace ConsoleRPGTests
{
    public class ItemTests
    {
        [Fact]
        public void AddWeapon_AddHighLevelWeaponToLowLevelCharacter_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new Warrior("Aragorn");
            Weapon weapon = new Weapon("Common axe", 2, Slot.Weapon, WeaponType.Axe, new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 });

            Assert.Throws<InvalidWeaponException>(() => warrior.addWeapon(weapon));
        }

        [Fact]
        public void AddArmor_AddHighLevelArmorToLowLevelCharacter_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new Warrior("Aragorn");
            Armor armor = new Armor("Common plate body armor", 2, Slot.Body, ArmorType.Plate, new PrimaryAttributes() { Strength = 1, Dexterity = 0, Intelligence = 0});

            Assert.Throws<InvalidArmorException>(() => warrior.addArmor(armor));
        }

        [Fact]
        public void AddWeapon_AddWrongWeaponTypeToCharacter_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new Warrior("Aragorn");
            Weapon weapon = new Weapon("Common bow", 1, Slot.Weapon, WeaponType.Bow, new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 });

            Assert.Throws<InvalidWeaponException>(() => warrior.addWeapon(weapon));
        }

        [Fact]
        public void AddArmor_AddWrongArmorTypeToCharacter_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new Warrior("Aragorn");
            Armor armor = new Armor("Common cloth head armor", 2, Slot.Head, ArmorType.Cloth, new PrimaryAttributes() { Strength = 0, Dexterity = 0, Intelligence = 5 });

            Assert.Throws<InvalidArmorException>(() => warrior.addArmor(armor));
        }

        [Fact]
        public void AddWeapon_AddValidWeaponToCharacter_ShouldReturnConfirmationMessage()
        {
            Warrior warrior = new Warrior("Aragorn");
            Weapon weapon = new Weapon("Common axe", 1, Slot.Weapon, WeaponType.Axe, new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 });
            
            string resultString = warrior.addWeapon(weapon);

            Assert.Equal("New weapon equipped! ", resultString);
        }

        [Fact]
        public void AddArmor_AddValidArmorToCharacter_ShouldReturnConfirmationMessage()
        {
            Warrior warrior = new Warrior("Aragorn");
            Armor armor = new Armor("Common plate body armor", 1, Slot.Body, ArmorType.Plate, new PrimaryAttributes() { Strength = 1, Dexterity = 0, Intelligence = 0 });

            string resultString = warrior.addArmor(armor);

            Assert.Equal("New armor equipped! ", resultString);
        }

        [Fact]
        public void CheckDamage_checkDamageOfCharacterWithoutWeapon_ShouldReturnExpectedDamage()
        {
            Warrior warrior = new Warrior("Aragorn");

            double characterDamage = warrior.Damage;

            Assert.Equal(1 * (1 + (5 / 100)), characterDamage);
        }

        [Fact]
        public void CheckDamage_checkDamageOfCharacterWithValidWeaponEquipped_ShouldReturnExpectedDamage()
        {
            Warrior warrior = new Warrior("Aragorn");
            Weapon weapon = new Weapon("Common axe", 1, Slot.Weapon, WeaponType.Axe, new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 });

            warrior.addWeapon(weapon);
            double characterDamage = warrior.CalculateCharacterDamage(warrior.Equipment);

            Assert.Equal((7 * 1.1) * (1 + (5 / 100)), characterDamage);
        }

        [Fact]
        public void CheckDamage_checkDamageOfCharacterWithValidWeaponAndArmorEquipped_ShouldReturnExpectedDamage()
        {
            Warrior warrior = new Warrior("Aragorn");
            Weapon weapon = new Weapon("Common axe", 1, Slot.Weapon, WeaponType.Axe, new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 });
            Armor armor = new Armor("Common plate body armor", 1, Slot.Body, ArmorType.Plate, new PrimaryAttributes() { Strength = 1, Dexterity = 0, Intelligence = 0 });

            warrior.addWeapon(weapon);
            warrior.addArmor(armor);
            double characterDamage = warrior.CalculateCharacterDamage(warrior.Equipment);

            Assert.Equal((7 * 1.1) * (1 + ((5 + 1) / 100)), characterDamage);
        }
    }
}
