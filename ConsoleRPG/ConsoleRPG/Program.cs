﻿using ConsoleRPG.CharacterTypes;
using ConsoleRPG.CommonAttributes;
using ConsoleRPG.Items;
using Sharprompt;
namespace ConsoleRPG
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mage mage = new Mage("Gandalf");
            Ranger ranger = new Ranger("Legolas");
            Rogue rogue = new Rogue("Gollum");
            Warrior warrior = new Warrior("Aragorn");

            Weapon mageWeapon = new Weapon("Common fire-staff", 2, Slot.Weapon, WeaponType.Staff, new WeaponAttributes() { Damage = 20, AttackSpeed = 0.5 });
            Armor mageArmor = new Armor("Common mage robe", 1, Slot.Body, ArmorType.Cloth, new PrimaryAttributes() { Strength = 0, Dexterity = 0, Intelligence = 2 });

            Weapon warriorWeapon = new Weapon("Common axe", 1, Slot.Weapon, WeaponType.Axe, new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 });
            Armor warriorArmor = new Armor("Common plate body armor", 1, Slot.Body, ArmorType.Plate, new PrimaryAttributes() { Strength = 1, Dexterity = 0, Intelligence = 0 });

            Weapon rangerWeapon = new Weapon("Common short bow", 3, Slot.Weapon, WeaponType.Bow, new WeaponAttributes() { Damage = 35, AttackSpeed = 1.5 });
            Armor rangerArmor = new Armor("Common leather robe", 3, Slot.Body, ArmorType.Leather, new PrimaryAttributes() { Strength = 0, Dexterity = 4, Intelligence = 0 });

            mage.LevelUp();

            mage.addWeapon(mageWeapon);
            mage.addArmor(mageArmor);

            warrior.addWeapon(warriorWeapon);
            warrior.addArmor(warriorArmor);

            //Throws exceptions
            //ranger.addWeapon(rangerWeapon);
            //ranger.addArmor(rangerArmor);

            warrior.PrintCharacterSheet();
            mage.PrintCharacterSheet();
            ranger.PrintCharacterSheet();
        }
    }
}
