﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CommonAttributes
{
    public struct WeaponAttributes
    {
        public int Damage { get; set; }

        public double AttackSpeed { get; set; }

        public WeaponAttributes(int damage, int attackSpeed)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
    }
}
