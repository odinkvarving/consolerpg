﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CommonAttributes
{
    /// <summary>
    /// struct used for representing the shared type PrimaryAttributes, used in both Character and Armor
    /// </summary>
    public struct PrimaryAttributes
    {
        public int Strength { get; set; }

        public int Dexterity { get; set; }

        public int Intelligence { get; set; }

        /// <summary>
        /// The constructor for the PrimaryAttributes class
        /// </summary>
        /// <param name="strength">Int representing the strength PrimaryAttribute</param>
        /// <param name="dexterity">Int representing the dexterity PrimaryAttribute</param>
        /// <param name="intelligence">Int representing the intelligence PrimaryAttribute</param>
        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
    }
}
