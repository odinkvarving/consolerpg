﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;
using ConsoleRPG.Items;

namespace ConsoleRPG.CharacterTypes
{
    /// <summary>
    /// Class inheriting from the Character class, representing the classtype of a Warrior
    /// </summary>
    public class Warrior : Character
    {
        /// <summary>
        /// Constructor for the Warrior class
        /// </summary>
        /// <param name="name">String representing the name of the Warrior</param>
        public Warrior(string name) : base(name, new List<WeaponType>() { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword }, new List<ArmorType>() { ArmorType.Mail, ArmorType.Plate }, 5, 2, 1)
        {

        }

        public override double CalculateCharacterDamage(Dictionary<Slot, Item> equipment)
        {
            double characterDamage = 1;
            int percentageIncrease = CalculatePercentageIncrease();
            int totalPrimaryAttribute = CalculateTotalPrimaryAttribute(PrimaryAttributes, Equipment);


            if (equipment.ContainsKey(Slot.Weapon))
            {
                Weapon weapon = (Weapon)equipment[Slot.Weapon];
                double weaponDPS = weapon.DPS();
                characterDamage = weaponDPS * (1 + (totalPrimaryAttribute / 100));
            }
            else
            {
                characterDamage = characterDamage * ((100 + percentageIncrease) / 100);
            }
            return characterDamage;
        }

        public override int CalculatePercentageIncrease()
        {
            return PrimaryAttributes.Strength;
        }

        public override int CalculateTotalPrimaryAttribute(PrimaryAttributes primaryAttributes, Dictionary<Slot, Item> equipment)
        {
            List<Item> armorList = new List<Item>();
            int basePrimaryAttributes = PrimaryAttributes.Strength;
            int equipmentPrimaryAttributes = 0;

            foreach (KeyValuePair<Slot, Item> entry in equipment)
            {
                if (entry.Key != Slot.Weapon)
                {
                    armorList.Add(entry.Value);
                }
            }
            foreach (Armor armor in armorList)
            {
                equipmentPrimaryAttributes += armor.PrimaryAttributes.Strength;
            }
            return equipmentPrimaryAttributes + basePrimaryAttributes;
        }

        public override void LevelUp()
        {
            PrimaryAttributes = new PrimaryAttributes(
                PrimaryAttributes.Strength + 3,
                PrimaryAttributes.Dexterity + 2,
                PrimaryAttributes.Intelligence + 1
                );
            Level++;

            Console.WriteLine($"Level up! {this.Name} has gained a level, and is now level {this.Level}. \r\n" +
                $"Primary attributes has increased to: \r\n" +
                $"Strength: {PrimaryAttributes.Strength} \r\n" +
                $"Dexterity: {PrimaryAttributes.Dexterity}\r\n" +
                $"Intelligence: {PrimaryAttributes.Intelligence}\r\n");
        }
    }
}
