﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;
using ConsoleRPG.Items;

namespace ConsoleRPG.CharacterTypes
{
    /// <summary>
    /// Class inheriting from the Character class, representing the classtype of a Mage
    /// </summary>
    public class Mage : Character
    {
        /// <summary>
        /// The constructor for the Mage class
        /// </summary>
        /// <param name="name">String representing the name of the Mage</param>
        public Mage(string name) : base(name, new List<WeaponType>() { WeaponType.Wand, WeaponType.Staff }, new List<ArmorType>() { ArmorType.Cloth }, 1, 1, 8)
        {

        }

        public override double CalculateCharacterDamage(Dictionary<Slot, Item> equipment)
        {
            double characterDamage = 1;
            int percentageIncrease = CalculatePercentageIncrease();
            int totalPrimaryAttribute = CalculateTotalPrimaryAttribute(PrimaryAttributes, Equipment);

            if (equipment.ContainsKey(Slot.Weapon))
            {
                Weapon weapon = (Weapon)equipment[Slot.Weapon];
                double weaponDPS = weapon.DPS();
                characterDamage = weaponDPS * (1 + (totalPrimaryAttribute / 100));
            }
            else
            {
                characterDamage = characterDamage * ((100 + percentageIncrease) / 100);
            }
            return characterDamage;
        }

        public override int CalculatePercentageIncrease()
        {
            return PrimaryAttributes.Intelligence;
        }

        public override int CalculateTotalPrimaryAttribute(PrimaryAttributes primaryAttributes, Dictionary<Slot, Item> equipment)
        {
            List<Item> armorList = new List<Item>();
            int basePrimaryAttributes = PrimaryAttributes.Intelligence;
            int equipmentPrimaryAttributes = 0;

            foreach (KeyValuePair<Slot, Item> entry in equipment)
            {
                if (entry.Key != Slot.Weapon)
                {
                    armorList.Add(entry.Value);
                }
            }
            foreach (Armor armor in armorList)
            {
                equipmentPrimaryAttributes += armor.PrimaryAttributes.Intelligence;
            }
            return equipmentPrimaryAttributes + basePrimaryAttributes;
        }

        public override void LevelUp()
        {
            PrimaryAttributes = new PrimaryAttributes(
                PrimaryAttributes.Strength + 1,
                PrimaryAttributes.Dexterity + 1,
                PrimaryAttributes.Intelligence + 5
                );
            Level++;

            Console.WriteLine($"Level up! {this.Name} has gained a level, and is now level {this.Level}. \r\n" +
                $"Primary attributes has increased to: \r\n" +
                $"Strength: {PrimaryAttributes.Strength} \r\n" +
                $"Dexterity: {PrimaryAttributes.Dexterity}\r\n" +
                $"Intelligence: {PrimaryAttributes.Intelligence}\r\n");
        }
    }
}
