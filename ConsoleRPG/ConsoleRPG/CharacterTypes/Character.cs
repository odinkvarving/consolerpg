﻿using ConsoleRPG.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;
using ConsoleRPG.Items;

namespace ConsoleRPG
{
    /// <summary>
    /// Abstract class which is the parent class to Mage, Ranger, Rogue and Warrior
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }

        public int Level { get; set; }

        public PrimaryAttributes PrimaryAttributes { get; set; }

        public int TotalPrimaryAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }

        public List<WeaponType> WeaponTypes { get; set; }

        public List<ArmorType> ArmorTypes { get; set; }

        public double Damage { get; set; }

        /// <summary>
        /// Constructor for the Character class
        /// </summary>
        /// <param name="name">A string representing the name of a Character</param>
        /// <param name="weaponTypes">A List of WeaponTypes that can be equipped by the Character</param>
        /// <param name="armorTypes">A List of ArmorTypes that can be equipped by the Character</param>
        /// <param name="strength">An integer representing the Strength attribute of the Character</param>
        /// <param name="dexterity">An integer representing the Dexterity attribute of the Character</param>
        /// <param name="intelligence">An integer representing the Intelligence attribute of the Character</param>
        public Character(string name, List<WeaponType> weaponTypes, List<ArmorType> armorTypes, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            WeaponTypes = weaponTypes;
            ArmorTypes = armorTypes;
            PrimaryAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
            TotalPrimaryAttributes = CalculateTotalPrimaryAttribute(PrimaryAttributes, Equipment);
            Damage = CalculateCharacterDamage(Equipment);
        }

        /// <summary>
        /// This method adds a Weapon to the Equipment-list of a Character.
        /// </summary>
        /// <param name="weapon"> The Weapon to add to the Equipment-list. </param>
        /// <returns> A string confirming that the Weapon was successfully equipped. </returns>
        /// <exception cref="InvalidWeaponException"> 
        /// Thrown when the Weapon that is equipped is of a higher level than the Character or the wrong WeaponType compared to the Characters CharacterType. 
        /// </exception>
        public string addWeapon(Weapon weapon)
        {
            CheckIfEmpty(weapon.Slot);

            if (weapon.Level > Level)
            {
                throw new InvalidWeaponException();
            }
            else if (!WeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException();
            }
            else
            {
                Equipment.Add(weapon.Slot, weapon);
                Console.WriteLine("New weapon equipped! \r\n");
                return "New weapon equipped! ";
            }
        }

        /// <summary>
        /// This method adds a piece of Armor to the Equipment-list of a Character.
        /// </summary>
        /// <param name="armor"> The Armor that is added to the Equipment-list. </param>
        /// <returns> A string confirming that Armor was successfully equipped. </returns>
        /// <exception cref="InvalidArmorException"> 
        /// Thrown when the piece of Armor that is equipped is of a higher level than the Character or is of the wrong ArmorType compared to the CharacterType.
        /// </exception>
        public string addArmor(Armor armor)
        {
            CheckIfEmpty(armor.Slot);

            if (armor.Level > Level)
            {
                throw new InvalidArmorException();
            }
            else if (!ArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException();
            }
            else
            {
                Equipment.Add(armor.Slot, armor);
                PrimaryAttributes = new PrimaryAttributes(
                    PrimaryAttributes.Strength + armor.PrimaryAttributes.Strength,
                    PrimaryAttributes.Dexterity + armor.PrimaryAttributes.Dexterity,
                    PrimaryAttributes.Intelligence + armor.PrimaryAttributes.Intelligence
                    );
                Console.WriteLine("New armor equipped! \r\n");
                return "New armor equipped! ";
            }
        }

        /// <summary>
        /// This method checks if there is already a piece of Armor or a Weapon equipped to a specific Slot and deleted the equipment at that Slot.
        /// </summary>
        /// <param name="slot"> The Slot that is checked. </param>
        public void CheckIfEmpty(Slot slot)
        {
            if (Equipment.ContainsKey(slot))
            {
                Equipment.Remove(slot);
            }
        }

        /// <summary>
        /// Abstract method used for calculating a Characters damage based on the Characters Equipment, Level, PrimaryAttributes and CharacterType-scaling.
        /// </summary>
        /// <param name="equipment"> The Equipment that is used in the calculation of the damage. </param>
        /// <returns> A double representing the damage of the Character. </returns>
        public abstract double CalculateCharacterDamage(Dictionary<Slot, Item> equipment);

        /// <summary>
        /// This method calculates the Total primary attributes of a Character based on the Characters PrimaryAttributes and its Equipments PrimaryAttributes
        /// </summary>
        /// <param name="primaryAttributes"> The PrimaryAttributes of the Character. </param>
        /// <param name="equipment"> The Characters Equipment. </param>
        /// <returns> An int representing the Characters Total primary attributes. </returns>
        public abstract int CalculateTotalPrimaryAttribute(PrimaryAttributes primaryAttributes, Dictionary<Slot, Item> equipment);

        /// <summary>
        /// Abstract method used for calculating a Characters percentage-increase in Damage based on what CharacterType the Character is and what level they are in their respective 'main' attributes. 
        /// </summary>
        /// <returns> An int representing the percentage-increase in Damage based on their main attribute. </returns>
        public abstract int CalculatePercentageIncrease();

        /// <summary>
        /// This method is used for increasing a Characters Level by one. 
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// This method prints a Characters statistics.
        /// </summary>
        /// <returns> A string containing the Characters statistics. </returns>
        public string PrintCharacterSheet()
        {
            StringBuilder sb = new StringBuilder("Character statistics: \r\n");
            sb.Append($"Name: {Name} \r\n");
            sb.Append($"Level: {Level} \r\n");
            sb.Append($"Strength: {PrimaryAttributes.Strength} \r\n");
            sb.Append($"Dexterity: {PrimaryAttributes.Dexterity} \r\n");
            sb.Append($"Intelligence: {PrimaryAttributes.Intelligence} \r\n");
            sb.Append($"Damage: {CalculateCharacterDamage(Equipment)} \r\n");

            Console.WriteLine(sb.ToString());
            return sb.ToString();
        }
    }
}
