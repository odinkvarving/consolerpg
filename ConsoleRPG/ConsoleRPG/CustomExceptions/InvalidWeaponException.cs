﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CustomExceptions
{
    public class InvalidWeaponException : Exception
    {
        public override string Message => "Error: Could not equip this weapon. Check character-level and Slot-type.  ";
    }
}
