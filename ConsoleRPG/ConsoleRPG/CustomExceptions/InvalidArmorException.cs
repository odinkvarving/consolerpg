﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.CustomExceptions
{
    public class InvalidArmorException : Exception
    {
        public override string Message => "Error: Could not equip this piece of armor. Check character level and Slot-type. ";
    }
}
