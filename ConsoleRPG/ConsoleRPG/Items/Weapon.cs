﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;

namespace ConsoleRPG.Items
{
    /// <summary>
    /// enum for representing the different WeaponTypes 
    /// </summary>
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
    }

    /// <summary>
    /// Weapon class inheriting from Item, where you can specify the WeaponType and WeaponAttributes
    /// </summary>
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }

        public WeaponAttributes WeaponAttributes { get; set; }

        /// <summary>
        /// The constructor for the Weapon class
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        /// <param name="weaponType"></param>
        /// <param name="weaponAttributes"></param>
        public Weapon(string name, int level, Slot slot, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(name, level, slot)
        {
            WeaponType = weaponType;
            WeaponAttributes = weaponAttributes;
        }

        /// <summary>
        /// Metod for calculating the DPS of a Weapon
        /// </summary>
        /// <returns>A double representing the DPS</returns>
        public double DPS()
        {
            return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
        }
    }
}
