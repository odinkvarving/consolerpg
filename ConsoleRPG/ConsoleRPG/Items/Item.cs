﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;

namespace ConsoleRPG.Items
{
    /// <summary>
    /// enum representing the Slot of an Item 
    /// </summary>
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon,
    }

    /// <summary>
    /// An abstract class representing an Item, which is the parent-class for Armor and Weapon
    /// </summary>
    public abstract class Item
    {
        public string Name { get; set; }

        public int Level { get; set; }

        public Slot Slot { get; set; }

        /// <summary>
        /// The constructor for the Item class
        /// </summary>
        /// <param name="name">String representing the name of he Item</param>
        /// <param name="level">Int representing the level of the Item</param>
        /// <param name="slot">Slot representing the slot of the item</param>
        public Item(string name, int level, Slot slot)
        {
            Name = name;
            Level = level;
            Slot = slot;
        }
    }
}
