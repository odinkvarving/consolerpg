﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.CommonAttributes;

namespace ConsoleRPG.Items
{
    /// <summary>
    /// enum representing the different ArmorTypes
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate,
    }

    /// <summary>
    /// Armor class inheriting from Item, where you can specify ArmorType and PrimaryAttributes for the armor
    /// </summary>
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes PrimaryAttributes { get; set; }

        /// <summary>
        /// The constructor for the Armor class
        /// </summary>
        /// <param name="name">String representing the name of the Armor</param>
        /// <param name="level">Int representing the level of the Armor</param>
        /// <param name="slot">Slot representing the slot of the Armor</param>
        /// <param name="armorType">ArmorType specifying the ArmorType of the Armor</param>
        /// <param name="primaryAttributes">PrimaryAttributes for the Armor</param>
        public Armor(string name, int level, Slot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) : base(name, level, slot)
        {
            ArmorType = armorType;
            PrimaryAttributes = primaryAttributes;
        }
    }
}
